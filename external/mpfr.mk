
MPFR_VERSION=mpfr-3.1.2

tars/mpfr.tar.xz: URL=https://ftp.gnu.org/gnu/mpfr/${MPFR_VERSION}.tar.xz
tars/mpfr.tar.xz: | tars/
	wget -nc ${URL} -O $@

mpfr/configured: mpfr/src/configure gmp/installed gawk/installed | mpfr/build/
	cd $|; ../src/configure --prefix=${PREFIX} --disable-static
	touch $@

mpfr/built: mpfr/configured
	make -C mpfr/build/ -j `nproc`
	touch $@

mpfr/installed: mpfr/built
	make -C mpfr/build/ install
	touch $@


