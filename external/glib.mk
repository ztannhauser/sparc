
tars/glib.tar.xz: URL=https://download.gnome.org/sources/glib/2.48/glib-2.48.0.tar.xz
tars/glib.tar.xz: | tars/
	wget -nc ${URL} -O $@

glib/configured: glib/src/configure | glib/build/
	cd $|; CFLAGS=-Wno-format-overflow ../src/configure \
		--prefix=${PREFIX} \
		--with-pcre=internal
	touch $@

glib/installed: glib/configured
	make -C glib/build -j `nproc`
	make -C glib/build install
	touch $@

























