
GMP_VERSION=gmp-6.0.0a

tars/gmp.tar.xz: URL=https://ftp.gnu.org/gnu/gmp/${GMP_VERSION}.tar.xz
tars/gmp.tar.xz: | tars/
	wget -nc ${URL} -O $@

gmp/configured: gmp/src/configure m4/installed | gmp/build/
	cd $|; ../src/configure --prefix=${PREFIX} --disable-static
	touch $@

gmp/built: gmp/configured
	make -C gmp/build/ -j `nproc`
	touch $@

gmp/installed: gmp/built
	make -C gmp/build/ install
	touch $@


