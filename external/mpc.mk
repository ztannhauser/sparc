
MPC_VERSION=mpc-1.0.2

tars/mpc.tar.gz: URL=https://ftp.gnu.org/gnu/mpc/${MPC_VERSION}.tar.gz
tars/mpc.tar.gz: | tars/
	wget -nc ${URL} -O $@

mpc/configured: mpc/src/configure gmp/installed mpfr/installed gawk/installed | mpc/build/
	cd $|; ../src/configure --prefix=${PREFIX} --disable-static
	touch $@

mpc/built: mpc/configured
	make -C mpc/build/ -j `nproc`
	touch $@

mpc/installed: mpc/built
	make -C mpc/build/ install
	touch $@


