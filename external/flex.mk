
tars/flex.tar.gz: URL=https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz
tars/flex.tar.gz: | tars/
	wget -nc ${URL} -O $@

flex/configured: flex/downloaded texinfo/installed m4/installed | flex/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

flex/built: flex/configured
	make -C flex/build/ -j `nproc`
	touch $@

flex/installed: flex/built
	make -C flex/build/ install
	ln -svf ${PREFIX}/bin/flex ${PREFIX}/bin/lex
	touch $@


