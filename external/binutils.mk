
BINUTILS_VERSION=binutils-2.24

tars/binutils.tar.gz: URL=https://ftp.gnu.org/gnu/binutils/${BINUTILS_VERSION}.tar.gz
tars/binutils.tar.gz: | tars/
	wget -nc ${URL} -O $@

# do I really need gawk/installed?
	# not for my laptop at least
	# not on my desktop either

binutils/configured: binutils/downloaded | binutils/build/
	cd $|; ../src/configure --prefix=${PREFIX} --target=${TARGET} \
		--disable-werror --disable-multilib
	touch $@

binutils/built: binutils/configured
	make -C binutils/build/ -j `nproc`
	touch $@

binutils/installed: binutils/built
	make -C binutils/build/ install
	touch $@

