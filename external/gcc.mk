
GCC_VERSION=gcc-9.2.0

tars/gcc.tar.gz: URL=https://ftp.gnu.org/gnu/gcc/${GCC_VERSION}/${GCC_VERSION}.tar.gz
tars/gcc.tar.gz: | tars/
	wget -nc ${URL} -O $@

gcc/patched: gcc/src/configure
	cd gcc/src; \
	sed -i.orig '/m64=/s/lib64/lib/' ./gcc/config/i386/t-linux64 && \
	rm libsanitizer -r
	touch $@

gcc/configured: \
	gcc/patched \
	binutils/installed \
	gawk/installed \
	gmp/installed \
	mpfr/installed \
	mpc/installed \
	isl/installed \
	cloog-isl/installed \
	| gcc/build/
	cd $|; ../src/configure \
		--prefix=${PREFIX} \
		--target=${TARGET} --with-cpu=v9 \
		--disable-threads \
		--disable-multilib \
		--with-system-zlib \
		--enable-languages=c,c++ \
		gcc_cv_libc_provides_ssp=yes
	touch $@

gcc/all-gcc-installed: gcc/configured
	make -C gcc/build -j `nproc` all-gcc
	make -C gcc/build install-gcc
	touch $@

gcc/libgcc-installed: glibc/startup-and-headers-installed
	make -C gcc/build -j `nproc` all-target-libgcc
	make -C gcc/build install-target-libgcc
	touch $@

gcc/fully-installed: glibc/fully-installed
	# make -C gcc/build
	make -C gcc/build -j `nproc`
	make -C gcc/build install
	touch $@


























