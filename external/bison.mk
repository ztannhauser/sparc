
tars/bison.tar.xz: URL=https://ftp.gnu.org/gnu/bison/bison-3.7.6.tar.xz
tars/bison.tar.xz: | tars/
	wget -nc ${URL} -O $@

bison/configured: bison/downloaded m4/installed | bison/build/
	cd $|; ../src/configure --prefix=${PREFIX}
	touch $@

bison/built: bison/configured
	make -C bison/build/ -j `nproc`
	touch $@

bison/installed: bison/built
	make -C bison/build/ install
	touch $@


