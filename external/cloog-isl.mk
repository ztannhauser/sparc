
CLOOG_VERSION=cloog-0.18.1

tars/cloog-isl.tar.gz: URL=ftp://gcc.gnu.org/pub/gcc/infrastructure/${CLOOG_VERSION}.tar.gz
tars/cloog-isl.tar.gz: | tars/
	wget -nc ${URL} -O $@

cloog-isl/configured: cloog-isl/src/configure gmp/installed gawk/installed | cloog-isl/build/
	cd $|; ../src/configure --prefix=${PREFIX} --disable-static
	touch $@

cloog-isl/built: cloog-isl/configured
	make -C cloog-isl/build/ -j `nproc`
	touch $@

cloog-isl/installed: cloog-isl/built
	make -C cloog-isl/build/ install
	touch $@


