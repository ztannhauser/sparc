
QEMU_VERSION=qemu-6.0.0

tars/qemu.tar.xz: URL=https://download.qemu.org/${QEMU_VERSION}.tar.xz
tars/qemu.tar.xz: | tars/
	wget -nc ${URL} -O $@

qemu/configured: \
	qemu/src/configure \
	ninja/installed \
	glib/installed | qemu/build/
	cd $|; ../src/configure --prefix=${PREFIX} --target-list=sparc32plus-linux-user
	touch $@

qemu/built: qemu/configured
	make -C qemu/build/ -j `nproc`
	touch $@

qemu/installed: qemu/built
	make -C qemu/build/ install
	touch $@


