
GLIBC_VERSION=glibc-2.20

tars/glibc.tar.xz: URL=https://ftp.gnu.org/gnu/glibc/${GLIBC_VERSION}.tar.xz
tars/glibc.tar.xz: | tars/
	wget -nc ${URL} -O $@

glibc/patched: glibc/src/configure
	cd ./glibc/src/sysdeps; \
	ln -svf sparcv9 ./unix/sysv/linux/sparc/sparc32/v9 && \
	sed -i "27i#include <sparc-nptl.h>" ./sparc/sparc32/sem_trywait.c
	touch $@

glibc/configured: \
	binutils/installed \
	glibc/patched \
	gawk/installed \
	gcc/all-gcc-installed \
	linux-headers/installed \
	| glibc/build/
	cd $|; CC=sparc-unknown-linux-gnu-gcc ../src/configure \
		--prefix=${PREFIX}/${TARGET} \
		--build=$$MACHTYPE \
		--host=${TARGET} \
		--target=${TARGET} --with-cpu=v9 \
		--disable-threads \
		--enable-add-ons \
		--enable-kernel=5.0.1 \
		--with-headers=${PREFIX}/${TARGET}/include \
		libc_cv_forced_unwind=yes libc_cv_ssp=no libc_cv_ssp_strong=no
	touch $@

glibc/startup-and-headers-installed: glibc/configured
	cd glibc/build; \
	make install-bootstrap-headers=yes install-headers && \
	make -j `nproc` csu/subdir_lib && \
	install csu/crt1.o csu/crti.o csu/crtn.o ${PREFIX}/${TARGET}/lib && \
	${TARGET}-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o ${PREFIX}/${TARGET}/lib/libc.so
	touch ${PREFIX}/${TARGET}/include/gnu/stubs.h
	touch $@

glibc/fully-installed: gcc/libgcc-installed
	make -C glibc/build -j `nproc`
	make -C glibc/build install
	touch $@

























