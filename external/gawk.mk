
tars/gawk.tar.xz: URL=https://ftp.gnu.org/gnu/gawk/gawk-5.1.0.tar.xz
tars/gawk.tar.xz: | tars/
	wget -nc ${URL} -O $@

gawk/configured: gawk/src/configure | gawk/build/
	cd $|; ../src/configure --prefix=${PREFIX} --disable-mpfr
	touch $@

gawk/built: gawk/configured
	make -C gawk/build/ -j `nproc`
	touch $@

gawk/installed: gawk/built
	make -C gawk/build/ install
	touch $@

