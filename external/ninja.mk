
tars/ninja.tar.gz: URL=https://github.com/ninja-build/ninja/archive/refs/tags/v1.10.2.tar.gz
tars/ninja.tar.gz: | tars/
	wget -nc ${URL} -O $@

ninja/built: ninja/src/configure
	cd ninja/src; python3 ./configure.py --bootstrap
	touch $@

ninja/installed: ninja/built
	cp ninja/src/ninja ./bin/ninja
	touch $@

