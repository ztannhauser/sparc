
ISL_VERSION=isl-0.12.2

tars/isl.tar.bz2: URL=ftp://gcc.gnu.org/pub/gcc/infrastructure/${ISL_VERSION}.tar.bz2
tars/isl.tar.bz2: | tars/
	wget -nc ${URL} -O $@

isl/configured: isl/src/configure gawk/installed gmp/installed | isl/build/
	cd $|; ../src/configure --prefix=${PREFIX} --disable-static
	touch $@

isl/built: isl/configured
	make -C isl/build/ -j `nproc`
	touch $@

isl/installed: isl/built
	make -C isl/build/ install
	touch $@


