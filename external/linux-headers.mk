
tars/linux-headers.tar.gz: URL=https://mirrors.edge.kernel.org/pub/linux/kernel/v5.x/linux-5.0.1.tar.gz
tars/linux-headers.tar.gz: | tars/
	wget -nc ${URL} -O $@

linux-headers/installed: linux-headers/src/configure | linux-headers/build/
	make -C linux-headers/src ARCH=sparc INSTALL_HDR_PATH=${PREFIX}/${TARGET} headers_install
	touch $@



