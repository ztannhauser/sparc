
struct cmdln_flags;

int read_cmdln(
	struct cmdln_flags** outgoing,
	int argc, char* const* argv);
