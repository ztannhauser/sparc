
#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "mylink.h"

int mylink(int fd, const char* to_path)
{
	int error = 0;
	char from_path[PATH_MAX];
	ENTER;
	
	dpv(fd);
	dpvs(to_path);
	snprintf(from_path, PATH_MAX, "/proc/self/fd/%d", fd);
	dpvs(from_path);
	
	assert(fd >= 0);
	
	if (unlink(to_path) < 0 && errno != ENOENT)
		fprintf(stderr, "%s unlink(\"%s\"): %m\n", argv0, to_path),
		error = e_syscall_failed;
	else if (linkat(AT_FDCWD, from_path, AT_FDCWD, to_path, AT_SYMLINK_FOLLOW) < 0)
		fprintf(stderr, "%s linkat(): %m\n", argv0),
		error = e_syscall_failed;
	
	EXIT;
	return error;
}


