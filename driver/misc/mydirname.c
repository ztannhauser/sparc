
#include <debug.h>
#include <error.h>

#include "mydirname.h"

static char retval[PATH_MAX];

char* mydirname(const char* path)
{
	ENTER;
	
	char* slash = rindex(path, '/');
	
	dpvs(path);
	
	if (!slash)
		strcpy(retval, ".");
	else if (slash == path)
		strcpy(retval, "/");
	else
		stpncpy(retval, path, slash - path);
	
	dpvs(retval);
	
	EXIT;
	return retval;
}

