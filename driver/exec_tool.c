

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include <string_array/struct.h>
#include <string_array/new.h>
#include <string_array/append.h>
#include <string_array/free.h>

#include "exec_tool.h"

static int exec(char* const* argv)
{
	int wstatus;
	pid_t child;
	int error = 0;
	ENTER;
	
	if (child = fork(), child < 0)
		fprintf(stderr, "%s: fork(): %m\n", argv0),
		error = e_syscall_failed;
	else if (!child)
	{
		if (execvp(argv[0], argv) < 0)
			fprintf(stderr, "%s: execvp(\"%s\"): %m\n", argv0, argv[0]),
			error = e_syscall_failed;
	}
	else if (waitpid(child, &wstatus, 0) < 0)
		fprintf(stderr, "%s: waitpid(): %m\n", argv0),
		error = e_syscall_failed;
	else if (wstatus)
		fprintf(stderr, "%s: '%s' returned with non-zero exit code!\n",
			argv0, argv[0]),
		error = e_subcommand_failed;
	
	EXIT;
	return error;
}

int exec_tool(
	int* retval_fd,
	char* tmpdir,
	char* tool_name,
	int input_fd,
	struct string_array* tool_args,
	bool verbose)
{
	int error = 0;
	ENTER;
	
	pid_t pid = getpid();
	
	dpv(pid);
	dpvs(tmpdir);
	dpvb(verbose);
	
	char input_path[PATH_MAX], output_file[PATH_MAX];
	
	int out_fd = open(tmpdir, O_TMPFILE | O_CLOEXEC | O_RDWR, 0664);
	
	dpv(out_fd);
	
	struct string_array* argv = NULL;
	
	if (out_fd < 0)
		fprintf(stderr, "%s: open(O_TMPFILE): %m\n", argv0),
		error = e_syscall_failed;
	else
	{
		sprintf(input_path, "/proc/%i/%i", pid, input_fd);
		sprintf(output_file, "/proc/%i/%i", pid, out_fd);
		
		error = 0
			?: new_string_array(&argv)
			?: string_array_append(argv, tool_name)
			?: string_array_append(argv, input_path)
			?: string_array_append(argv, "-o")
			?: string_array_append(argv, output_file)
			;
	}
	
	size_t i, n;
	for (i = 0, n = tool_args->n; !error && i < n; i++)
		error = string_array_append(argv, tool_args->data[i]);
	
	if (!error)
		error = string_array_append(argv, NULL);
	
	if (!error)
	{
		if (verbose)
		{
			printf("$");
			for (i = 0, n = argv->n - 1; i < n; i++)
				printf(" '%s'", argv->data[i]);
			puts("");
		}
		error = exec(argv->data);
	}
	
	if (!error)
		*retval_fd = out_fd;
	else if (out_fd > 0)
		close(out_fd);
	
	free_string_array(argv);
	
	EXIT;
	return error;
}










