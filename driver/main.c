
#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include <defines/CPP.h>
#include <defines/CC.h>
#include <defines/AS.h>

#include <cmdln/struct.h>
#include <cmdln/process.h>
#include <cmdln/free.h>

#include <string_array/struct.h>
#include <string_array/new.h>
#include <string_array/free.h>
#include <string_array/free_each.h>

#include <string/strendswith.h>

#include <memory/scalloc.h>

#include <misc/mydirname.h>
#include <misc/mylink.h>

#include "exec_tool.h"

#if DEBUGGING
int debug_depth;
#endif

int main(int argc, char* const* argv)
{
	int error = 0;
	ENTER;
	
	struct cmdln_flags* flags = NULL;
	
	error = process_cmdln(&flags, argc, argv);
	
	int* object_fds = NULL;
	char* tmpdir, *input, suffix;
	size_t i, n = 0;
	
	if (!error)
	{
		n = flags->inputs->n;
		tmpdir = flags->output_file ? mydirname(flags->output_file) : ".";
		error = scalloc((void**) &object_fds, sizeof(int), n);
	}
	
	for (i = 0; !error && i < n; i++)
	{
		int fd_in = -1, fd_cpp = -1, fd_cc = -1, fd_as = -1;
		
		input = flags->inputs->data[i];
		suffix = rindex(input, '.')[1];
		
		if ((fd_in = open(input, O_CLOEXEC | O_RDONLY)) < 0)
			fprintf(stderr, "%s: open(\"%s\"): %m\n", argv0, input),
			error = e_open_input_failed;
		
		if (!error)
		{
			// possibly call preprocessor:
			if (sa_preprocessor <= flags->stop_after && (suffix == 'c' || suffix == 'S'))
				suffix = suffix == 'c' ? 'i' : 's',
				error = exec_tool(&fd_cpp, tmpdir, CPP, fd_in,
					flags->preprocessor_args, flags->verbose);
			else if ((fd_cpp = dup(fd_in)) < 0)
				fprintf(stderr, "%s: dup(): %m\n", argv0),
				error = e_syscall_failed;
		}
		
		if (!error)
		{
			// possibly call compiler:
			if (sa_compiler <= flags->stop_after && suffix == 'i')
				suffix = 's',
				error = exec_tool(&fd_cc, tmpdir, CC, fd_cpp,
					flags->compiler_args, flags->verbose);
			else if ((fd_cc = dup(fd_cpp)) < 0)
				fprintf(stderr, "%s: dup(): %m\n", argv0),
				error = e_syscall_failed;
		}
		
		if (!error)
		{
			// possibly call assembler:
			if (sa_assembler <= flags->stop_after && suffix == 's')
				suffix = 'o',
				error = exec_tool(&fd_as, tmpdir, AS, fd_cc,
					flags->assembler_args, flags->verbose);
			else if ((fd_as = dup(fd_cc)) < 0)
				fprintf(stderr, "%s: dup(): %m\n", argv0),
				error = e_syscall_failed;
		}
		
		// possibly linkat() tempfile:
		if (!error)
		{
			if (flags->stop_after < sa_linker)
			{
				char name[PATH_MAX], *slash;
				
				if (flags->output_file)
					strcpy(name, flags->output_file);
				else if ((slash = rindex(input, '.')))
					sprintf(name, "%*s.%c", (int)(slash - input), input, suffix);
				else
					sprintf(name, "%s.%c", input, suffix);
				
				dpvs(name);
				
				error = mylink(fd_as, name);
			}
			else
			{
				object_fds[i] = fd_as;
				fd_as = -1;
			}
		}
		
		if (fd_in  > 0) close(fd_in);
		if (fd_cpp > 0) close(fd_cpp);
		if (fd_cc  > 0) close(fd_cc);
		if (fd_as  > 0) close(fd_as);
	}
	
	// possibly link:
	if (!error && sa_linker <= flags->stop_after)
	{
		TODO;
	}
	
	// possibly linkat() tempfile:
	if (!error && sa_linker == flags->stop_after)
	{
		// char* name = output_file ?: "a.out";
		TODO;
	}
	
	// possibly execute:
	if (!error && sa_linker <= flags->stop_after)
	{
		TODO;
	}
	
	while (n--)
		if (object_fds[n] > 0)
			close(object_fds[n]);
	
	free(object_fds);
	
	free_cmdln_flags(flags);
	
	EXIT;
	return error;
}











