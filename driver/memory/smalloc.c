
#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "smalloc.h"

int smalloc(void** out, size_t size)
{
	int error = 0;
	ENTER;
	
	void* ptr = malloc(size);
	
	if (!ptr)
	{
		fprintf(stderr, "%s: malloc(%lu): %m\n", argv0, size);
		error = e_out_of_memory;
	}
	else
		*out = ptr;
	
	EXIT;
	return error;
}

