
#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "scalloc.h"

int scalloc(void** out, size_t nmemb, size_t size)
{
	int error = 0;
	ENTER;
	
	void* ptr = calloc(nmemb, size);
	
	if (!ptr)
	{
		fprintf(stderr, "%s: calloc(%lu, %lu): %m\n", argv0, nmemb, size);
		error = e_out_of_memory;
	}
	else
		*out = ptr;
	
	EXIT;
	return error;
}

