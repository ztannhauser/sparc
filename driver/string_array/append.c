
#include <debug.h>

#include <memory/srealloc.h>

#include "struct.h"
#include "append.h"

int string_array_append(struct string_array* this, char* appendme)
{
	int error = 0;
	ENTER;
	
	dpvs(appendme);
	
	if (this->n + 1 >= this->cap)
	{
		size_t newcap = this->cap * 2 ?: 1;
		
		error = srealloc((void**) &this->data, sizeof(*this->data) * newcap);
		
		if (!error)
			this->cap = newcap;
	}
	
	if (!error)
	{
		this->data[this->n++] = appendme;
	}
	
	EXIT;
	return error;
}

