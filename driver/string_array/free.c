
#include <debug.h>

#include "struct.h"
#include "free.h"

void free_string_array(struct string_array* this)
{
	ENTER;
	if (this)
	{
		free(this->data);
		free(this);
	}
	EXIT;
}

