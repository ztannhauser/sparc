
#include <debug.h>

#include <memory/smalloc.h>

#include "struct.h"
#include "new.h"

int new_string_array(struct string_array** new)
{
	int error = 0;
	ENTER;
	
	struct string_array* this = NULL;
	
	error = smalloc((void**) &this, sizeof(*this));
	
	if (!error)
	{
		this->data = NULL;
		this->n = this->cap = 0;
		
		*new = this;
	}
	
	EXIT;
	return error;
}

