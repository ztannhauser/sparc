
#include <debug.h>

#include <string_array/free.h>

#include "struct.h"
#include "free.h"

void free_cmdln_flags(struct cmdln_flags* flags)
{
	ENTER;
	
	if (flags)
	{
		free_string_array(flags->inputs);
		
		free_string_array(flags->preprocessor_args);
		free_string_array(flags->compiler_args);
		free_string_array(flags->assembler_args);
		free_string_array(flags->linker_args);
		free_string_array(flags->execute_args);
		
		free(flags);
	}
	
	EXIT;
}

