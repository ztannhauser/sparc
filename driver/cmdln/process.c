
#include <getopt.h>

#include <error.h>
#include <debug.h>

#include <defines/argv0.h>

#include <memory/smalloc.h>

#include <string_array/struct.h>
#include <string_array/new.h>
#include <string_array/append.h>
#include <string_array/free.h>
#include <string_array/free_each.h>

#include "struct.h"
#include "process.h"

static struct option long_options[] = {
	{"std",     required_argument, 0,  0},
	{0,         0,                 0,  0}
};

int process_cmdln(struct cmdln_flags** out, int argc, char* const* argv)
{
	int error = 0;
	ENTER;
	
	enum stop_after stop_after = sa_linker;
	const char* output_file = NULL;
	
	struct string_array* inputs = NULL;
	struct string_array* preprocessor_args = NULL;
	struct string_array* compiler_args = NULL;
	struct string_array* assembler_args = NULL;
	struct string_array* linker_args = NULL;
	struct string_array* execute_args = NULL;
	
	bool verbose = false;
	
	error = 0
		?: new_string_array(&inputs)
		?: new_string_array(&preprocessor_args)
		?: new_string_array(&compiler_args)
		?: new_string_array(&assembler_args)
		?: new_string_array(&linker_args)
		?: new_string_array(&execute_args);
	
	int opt, option_index;
	while (!error && (opt = getopt_long(argc, argv, "EScxo:D:I:W:l:v", long_options, &option_index)) != -1)
	{
		switch (opt ?: option_index)
		{
			case 0:
				fprintf(stderr, "%s: the only support standard is c99\n", argv0);
				break;
			
			case 'E': stop_after = sa_preprocessor; break;
			case 'S': stop_after = sa_assembler; break;
			case 'c': stop_after = sa_compiler; break;
			case 'x': stop_after = sa_execute; break;
			
			case 'o': output_file = optarg; break;
			
			case 'D':
				TODO; // append to preprocessor_args
				break;
			
			case 'I':
				TODO; // append to preprocessor_args
				break;
			
			case 'W':
				TODO; // append to compiler args
				break;
			
			case 'L':
				TODO; // append to linker args
				break;
			
			case 'l':
				TODO; // append to linker args
				break;
			
			case 'v':
				verbose = true;
				break;
			
			default:
			{
				fprintf(stderr, "Usage: %s [args]\n", argv0);
				error = e_bad_command_line_args;
				break;
			}
		}
	}
	
	while (!error && optind < argc)
		error = string_array_append(inputs, argv[optind++]);
	
	if (!error && output_file && stop_after < sa_linker && inputs->n > 1)
	{
		fprintf(stderr, "%s: cannot specify ‘-o’ with ‘-c’, ‘-S’ or ‘-E’ with "
			"multiple files\n", argv0);
		error = e_bad_command_line_args;
	}
	
	struct cmdln_flags* flags = NULL;
	
	if (!error)
		error = smalloc((void**) &flags, sizeof(*flags));
	
	if (!error)
	{
		flags->stop_after = stop_after;
		flags->output_file = output_file;
		
		flags->inputs = inputs, inputs = NULL;
		
		flags->preprocessor_args = preprocessor_args, preprocessor_args = NULL;
		flags->compiler_args = compiler_args, compiler_args = NULL;
		flags->assembler_args = assembler_args, assembler_args = NULL;
		flags->linker_args = linker_args, linker_args = NULL;
		flags->execute_args = execute_args, execute_args = NULL;
		
		flags->verbose = verbose;
		
		*out = flags;
	}
	
	free_string_array(inputs);
	free_string_array(preprocessor_args);
	free_string_array(compiler_args);
	free_string_array(assembler_args);
	free_string_array(linker_args);
	free_string_array(execute_args);
	
	EXIT;
	return error;
}




















