
struct string_array;

struct cmdln_flags
{
	bool verbose;
	
	enum stop_after {
		sa_preprocessor,
		sa_compiler,
		sa_assembler,
		sa_linker,
		sa_execute,
	} stop_after;
	
	const char* output_file;
	
	struct string_array* inputs;
	
	struct string_array* preprocessor_args;
	struct string_array* compiler_args;
	struct string_array* assembler_args;
	struct string_array* linker_args;
	struct string_array* execute_args;
};

