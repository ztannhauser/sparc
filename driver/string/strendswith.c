
#include <debug.h>

#include <macros/strequals.h>

#include "strendswith.h"

bool strendswith(const char* str, const char* suffix)
{
	bool retval;
	ENTER;
	
	dpvs(str);
	dpvs(suffix);
	
	size_t a = strlen(str), b = strlen(suffix);
	retval = a >= b && strequals(str + a - b, suffix);
	
	EXIT;
	return retval;
}

