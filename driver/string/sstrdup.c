
#include <string.h>

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "sstrdup.h"

int sstrdup(char** out, const char* str)
{
	int error = 0;
	ENTER;
	
	assert(str);
	
	char* ptr = strdup(str);
	
	if (!ptr)
	{
		fprintf(stderr, "%s: strdup(): %m\n", argv0);
		error = e_out_of_memory;
	}
	else
		*out = ptr;
	
	EXIT;
	return error;
}

