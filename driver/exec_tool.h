
struct string_array;

int exec_tool(
	int* retval_fd,
	char* tmpdir,
	char* tool_name,
	int input_fd,
	struct string_array* tool_args,
	bool verbose);
