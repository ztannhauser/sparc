
# relevant PDFs online:
pdfs:
	mkdir pdfs

pdfs/802-1947.pdf: PDF_URL = https://docs.oracle.com/cd/E19695-01/802-1947/802-1947.pdf

pdfs/sparcv9.pdf: PDF_URL = https://cr.yp.to/2005-590/sparcv9.pdf

pdfs/%.pdf: | pdfs
	curl ${PDF_URL} -o $@

