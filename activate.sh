set -v
export PATH="${PATH:+${PATH}:}$PWD/external/bin"
export MANPATH="${MANPATH:+${MANPATH}:}$PWD/external/share/man"
export C_INCLUDE_PATH="${C_INCLUDE_PATH:+${C_INCLUDE_PATH}:}$PWD/external/include"
export CPLUS_INCLUDE_PATH="${CPLUS_INCLUDE_PATH:+${CPLUS_INCLUDE_PATH}:}$PWD/external/include"
export LIBRARY_PATH="${LIBRARY_PATH:+${LIBRARY_PATH}:}$PWD/external/lib"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}:}$PWD/external/lib"
export PKG_CONFIG_PATH="${PKG_CONFIG_PATH:+${PKG_CONFIG_PATH}:}$PWD/external/sslib/pkgconfig"
set +v

