
EXAMPLE_CCP = false
EXAMPLE_CCP = cpp

EXAMPLE_CC = false
EXAMPLE_CC = false

EXAMPLE_AS = false
EXAMPLE_AS = false

EXAMPLE_LD = false
EXAMPLE_LD = false

bin/examples/%.i: examples/%.c | bin
	${EXAMPLE_CPP} $< -o $@

bin/examples/%.s: examples/%.sx | bin
	${EXAMPLE_CPP} $< -o $@

bin/examples/%.s: examples/%.i | bin
	${EXAMPLE_CC} $< -o $@

bin/examples/%.o: examples/%.s | bin
	${EXAMPLE_AS} $< -o $@

bin/examples/%: examples/%.o | bin
	${EXAMPLE_LD} $< -o $@



