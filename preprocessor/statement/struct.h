
#include "kind.h"

struct statement
{
	enum statement_kind kind;
	
	struct statement* next;
};
