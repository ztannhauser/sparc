
#include <stddef.h>

struct statement;

#include "kind.h"

int new_statement(
	struct statement** new,
	enum statement_kind kind,
	size_t size);
