
#include <debug.h>

#include <memory/smalloc.h>

#include "struct.h"
#include "new.h"

int new_statement(
	struct statement** new,
	enum statement_kind kind,
	size_t size)
{
	int error = 0;
	ENTER;
	
	struct statement* this = NULL;
	
	error = smalloc((void**) &this, size);
	
	if (!error)
	{
		this->kind = kind;
		
		*new = this;
	}
	
	EXIT;
	return error;
}

