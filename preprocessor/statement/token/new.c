
#include <debug.h>

#include <statement/new.h>

#include "struct.h"
#include "new.h"

int new_token_statement(
	struct token_statement** new,
	char token)
{
	int error = 0;
	ENTER;
	
	struct token_statement* this = NULL;
	
	error = new_statement(
		(struct statement**) &this, sk_token, sizeof(*this));
	
	if (!error)
	{
		this->token = token;
		
		*new = this;
	}
	
	EXIT;
	return error;
}

