
#include <debug.h>

#include <statement/new.h>

#include "struct.h"
#include "new.h"

int new_macro_invocation_statement(
	struct macro_invocation_statement** new,
	char* macro_name)
{
	int error = 0;
	ENTER;
	
	struct macro_invocation_statement* this = NULL;
	
	error = new_statement(
		(struct statement**) &this, sk_macro_invocation, sizeof(*this));
	
	if (!error)
	{
		this->macro_name = macro_name;
		
		*new = this;
	}
	
	EXIT;
	return error;
}

