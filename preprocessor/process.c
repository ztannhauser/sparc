
#include <debug.h>

#include <parse/parse.h>

#include "process.h"

int process(const char* path, int out)
{
	int error = 0;
	struct statement* tree = NULL;
	ENTER;
	
	// call parse(path), get expression tree
	error = parse(&tree, path);
	TODO;
	
	// call evaluate(tree)
	TODO;
	
	// destruct tree
	TODO;
	
	EXIT;
	return error;
}

