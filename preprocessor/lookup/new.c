
#include <debug.h>

#include <memory/smalloc.h>

#include "struct.h"
#include "new.h"

int new_lookup(struct lookup** new)
{
	int error = 0;
	ENTER;
	
	struct lookup* this = NULL;
	
	error = smalloc((void**) &this, sizeof(*this));
	
	if (!error)
	{
		*new = this;
	}
	
	EXIT;
	return error;
}

