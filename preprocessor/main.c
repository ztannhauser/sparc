
#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include <cmdln/struct.h>
#include <cmdln/process.h>
#include <cmdln/free.h>

#include <string_array/struct.h>

#include <lookup/new.h>
#include <lookup/free.h>

#include <process.h>

#if DEBUGGING
int debug_depth;
#endif

int main(int argc, char* const* argv)
{
	int error = 0;
	size_t i, n;
	int out_fd = 1;
	struct cmdln_flags* flags = NULL;
	struct lookup* lookup = NULL;
	ENTER;
	
	error = 0
		?: process_cmdln(&flags, argc, argv)
		?: new_lookup(&lookup);
	
	// open output
	if (!error && flags->output_file && (out_fd = creat(flags->output_file, 0664)) < 0)
		fprintf(stderr, "%s: creat(\"%s\"): %m\n", argv0, flags->output_file),
		error = e_open_output_failed;
	
	dpv(out_fd);
	
	// define values from command line:
	if (!error)
	for (i = 0, n = flags->defines->n; !error && i < n; i++)
	{
		dpvs(flags->defines->data[i]);
		TODO;
	}
	
	// call reader on each input file
	if (!error)
	for (i = 0, n = flags->inputs->n; !error && i < n; i++)
	{
		dpvs(flags->inputs->data[i]);
		
		error = process(flags->inputs->data[i], out_fd);
		
		TODO;
	}
	
	free_lookup(lookup);
	free_cmdln_flags(flags);
	
	if (out_fd > 1) close(out_fd);
	
	EXIT;
	return error;
}














