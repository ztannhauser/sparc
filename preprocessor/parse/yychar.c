
// vim: noai:ts=4:sw=4

#include <debug.h>

#include "yyscan_t.h"
#include "yybyte.h"
#include "yychar.h"

int yychar(struct yyscan_t* s)
{
	int error = 0;
	ENTER;
	
	
	if (s->cb == '?') // check for trigraphs
	{
		TODO;
	}
	else
	{
		s->cc = s->cb;
		error = yybyte(s);
	}
	
	dpvc(s->cc);
	
	EXIT;
	return error;
}

