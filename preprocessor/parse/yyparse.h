/* A Bison parser, made by GNU Bison 3.7.6.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_PREPROCESSOR_PARSE_YYPARSE_H_INCLUDED
# define YY_YY_PREPROCESSOR_PARSE_YYPARSE_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    t_newline = 258,               /* t_newline  */
    t_identifier = 259,            /* t_identifier  */
    t_integer_literal = 260,       /* t_integer_literal  */
    t_string_literal = 261,        /* t_string_literal  */
    t_character_literal = 262,     /* t_character_literal  */
    t_times = 263,                 /* t_times  */
    t_divide = 264,                /* t_divide  */
    t_rdivide = 265,               /* t_rdivide  */
    t_plus = 266,                  /* t_plus  */
    t_minus = 267,                 /* t_minus  */
    t_leftshift = 268,             /* t_leftshift  */
    t_rightshift = 269,            /* t_rightshift  */
    t_greater_than = 270,          /* t_greater_than  */
    t_greater_than_or_equal_to = 271, /* t_greater_than_or_equal_to  */
    t_less_than = 272,             /* t_less_than  */
    t_less_than_or_equal_to = 273, /* t_less_than_or_equal_to  */
    t_equal_to = 274,              /* t_equal_to  */
    t_not_equal_to = 275,          /* t_not_equal_to  */
    t_bitwise_and = 276,           /* t_bitwise_and  */
    t_bitwise_or = 277,            /* t_bitwise_or  */
    t_bitwise_xor = 278,           /* t_bitwise_xor  */
    t_logical_and = 279,           /* t_logical_and  */
    t_logical_or = 280,            /* t_logical_or  */
    t_question_mark = 281,         /* t_question_mark  */
    t_pound = 282,                 /* t_pound  */
    t_poundpound = 283,            /* t_poundpound  */
    t_define = 284,                /* t_define  */
    t_include = 285,               /* t_include  */
    t_if = 286,                    /* t_if  */
    t_else = 287,                  /* t_else  */
    t_elif = 288,                  /* t_elif  */
    t_endif = 289,                 /* t_endif  */
    t_comma = 290,                 /* t_comma  */
    t_colon = 291,                 /* t_colon  */
    t_semicolon = 292,             /* t_semicolon  */
    t_open_paren = 293,            /* t_open_paren  */
    t_close_paren = 294,           /* t_close_paren  */
    t_unrecognized_token = 295,    /* t_unrecognized_token  */
    last_token = 296               /* last_token  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 20 "preprocessor/parse/yyparse.y"

	char unrecognized_token;
	char* identifier;
	struct statement* statement;
	struct token_statement* token;
	struct macro_invocation_statement* macro_invocation;

#line 113 "preprocessor/parse/yyparse.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int yyparse (struct statement** result, int* error, struct yyscan_t* scanner);

#endif /* !YY_YY_PREPROCESSOR_PARSE_YYPARSE_H_INCLUDED  */
