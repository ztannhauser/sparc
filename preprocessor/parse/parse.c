
// vim: noai:ts=4:sw=4

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "yyscan_t.h"

#include "yybyte.h"
#include "yychar.h"
#include "yylex.h"
#include "yyparse.h"

#include "parse.h"

int parse(struct statement** out, const char* path)
{
	int error = 0;
	struct yyscan_t b = {-1, 0, 0};
	ENTER;
	
	if ((b.in = open(path, O_RDONLY)) < 0)
		fprintf(stderr, "%s: open(\"%s\"): %m\n", argv0, path),
		error = e_open_input_failed;
	else
		error = yybyte(&b) ?: yychar(&b);
	
	// struct expression_tree* result = NULL;
	if (!error)
		switch (yyparse(out, &error, &b))
		{
			case 1: error = error ?: e_bison_error;
			case 2: error = e_out_of_memory;
		}
	
	if (b.in > 0)
		close(b.in);
	
	EXIT;
	return error;
}





















