
// vim: noai:ts=4:sw=4

#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "yyscan_t.h"
#include "yybyte.h"

int yybyte(struct yyscan_t* s)
{
	int error = 0;
	ssize_t read_retval;
	ENTER;
	
	if (s->i < s->n)
		s->cb = s->buffer[s->i++];
	else if ((read_retval = read(s->in, s->buffer, sizeof(s->buffer))) < 0)
		fprintf(stderr, "%s: read(): %m\n", argv0),
		error = e_syscall_failed;
	else if (!read_retval)
		s->cb = '\0'; // indicate EOF
	else
		s->i = 0, s->n = read_retval,
		s->cb = s->buffer[s->i++];
	
	if (!error)
		dpvc(s->cb);
	
	EXIT;
	return error;
}

