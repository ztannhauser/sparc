
#include <debug.h>

#include <memory/srealloc.h>

#include <macros/strequals.h>

#include "yyscan_t.h"
#include "yychar.h"
#include "yyparse.h"
#include "yylex.h"

enum state
{
	s_start = last_token,
	
	s_after_newline,
	s_after_slash,
	s_after_open_paren,
	s_after_close_paren,
	s_after_pound,
	s_after_poundpound,
	
	number_of_states,
};

#define ALL 0 ... 127
static int tokenizer[number_of_states][128] = {
	
	[s_start][ALL] = t_unrecognized_token,
	
	[s_start]['\0'] = YYEOF,
	
	[s_start][ ' '] = s_start,
	[s_start]['\t'] = s_start,
	
	[s_start]['\n'] = s_after_newline,
		[s_after_newline][ALL] = t_newline,
	
	[s_start]['\\'] = s_after_slash,
		[s_after_slash]['\n'] = s_start,
	
	[s_start]['('] = s_after_open_paren,
		[s_after_open_paren][ALL] = t_open_paren,
		
	[s_start][')'] = s_after_close_paren,
		[s_after_close_paren][ALL] = t_close_paren,
	
	[s_start][ '#'] = s_after_pound,
		[s_after_pound][ALL] = t_pound,
		[s_after_pound]['#'] = s_after_poundpound,
			[s_after_poundpound][ALL] = t_poundpound,
	
	[s_start]['_'] = t_identifier,
	[s_start]['A' ... 'Z'] = t_identifier,
	[s_start]['a' ... 'z'] = t_identifier,
};

static const char* identifier_chars =
	"_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

static int yylex_unrecognized_token(enum yytokentype* t, union YYSTYPE* ts, struct yyscan_t* s)
{
	int error = 0;
	ENTER;
	
	dpvc(s->cc);
	
	*t = t_unrecognized_token;
	ts->unrecognized_token = s->cc;
	error = yychar(s);
	
	CHECK;
	
	EXIT;
	return error;
}

static int yylex_identifier(enum yytokentype* t, union YYSTYPE* ts, struct yyscan_t* s)
{
	int error = 0;
	ENTER;
	
	assert(index(identifier_chars, s->cc));
	
	struct {
		char* data;
		size_t n, cap;
	} identifier = {NULL, 0, 0};
	
	size_t new_cap;
	
	while (!error && index(identifier_chars, s->cc))
	{
		new_cap = identifier.cap * 2 ?: 1;
		
		if (identifier.n + 1 >= identifier.cap)
			error = srealloc((void**) &identifier.data, new_cap);
		
		if (!error)
		{
			identifier.cap = new_cap;
			identifier.data[identifier.n++] = s->cc;
			error = yychar(s);
		}
	} 
	
	if (!error && identifier.n + 1 >= identifier.cap)
		error = srealloc((void**) &identifier.data, identifier.cap * 2);
	
	if (!error)
	{
		identifier.data[identifier.n++] = '\0';
		
		dpvs(identifier.data);
		
		if (strequals(identifier.data, "include"))
			*t = t_include;
		else
			ts->identifier = identifier.data,
			identifier.data = NULL;
	}
	
	free(identifier.data);
	
	EXIT;
	return error;
}

int yylex(union YYSTYPE* ts, struct yyscan_t* s)
{
	int error = 0;
	enum yytokentype t;
	ENTER;
	
	dpvc(s->cc);
	
	int state = s_start;
	
	while (!error && (state = tokenizer[state][s->cc]) >= s_start)
		error = yychar(s);
	
	assert(error || state < s_start);
	
	if (!error)
		switch (state)
		{
			case YYerror:
				TODO;
				break;
			
			case t_unrecognized_token:
				error = yylex_unrecognized_token(&t, ts, s);
				break;
			
			case t_identifier:
				error = yylex_identifier(&t, ts, s);
				break;
			
			case YYEOF:
			default:
				dpv(state);
				t = state;
				break;
		}
	
	EXIT;
	return error ? YYerror : t;
}


























