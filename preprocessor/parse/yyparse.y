
%{

#include <debug.h>

#include "yylex.h"
#include "yyerror.h"

#include <statement/struct.h>
#include <statement/token/new.h>
#include <statement/token/free.h>
#include <statement/macro_invocation/new.h>
#include <statement/macro_invocation/free.h>

struct yyscan_t;
struct statement;

%}

%union {
	char unrecognized_token;
	char* identifier;
	struct statement* statement;
	struct token_statement* token;
	struct macro_invocation_statement* macro_invocation;
}

%define api.pure full
%define parse.error detailed

%lex-param {struct yyscan_t* scanner}

%parse-param {struct statement** result}
%parse-param {int* error}
%parse-param {struct yyscan_t* scanner}

%token t_newline;

%token <identifier> t_identifier;

%token t_integer_literal t_string_literal t_character_literal;
%token t_times t_divide t_rdivide;
%token t_plus t_minus;
%token t_leftshift t_rightshift;
%token t_greater_than t_greater_than_or_equal_to;
%token t_less_than t_less_than_or_equal_to;
%token t_equal_to t_not_equal_to;
%token t_bitwise_and t_bitwise_or t_bitwise_xor;
%token t_logical_and t_logical_or t_question_mark;

%token t_pound t_poundpound t_define t_include t_if t_else t_elif t_endif;
%token t_comma t_colon t_semicolon;
%token t_open_paren t_close_paren;

%token<unrecognized_token> t_unrecognized_token;

%type<token> token_statement;
%type<statement> statement statements
%type<macro_invocation> macro_invocation

%token last_token;

%start start

%destructor {free($$);} <identifier>

%destructor {free_macro_invocation($$);} <macro_invocation>

%%

token_statement:
	t_unrecognized_token {
		CHECK;
		if ((*error = new_token_statement(&$$, $1)))
			YYABORT;
	}
	| t_newline {
		if ((*error = new_token_statement(&$$, '\n')))
			YYABORT;
	}
	;

expression1:
	t_identifier
	| t_integer_literal
	| t_string_literal
	| t_pound t_identifier {dpvs($2); TODO;}
	| t_identifier t_poundpound t_identifier {dpvs($1); dpvs($3); TODO;}
	| t_open_paren expressionC t_close_paren;

expression2:
	expression1
	| expression1 t_times expression2
	| expression1 t_divide expression2
	| expression1 t_rdivide expression2
	;

expression3:
	expression2
	| expression2 t_plus expression3
	| expression2 t_minus expression3
	;

expression4:
	expression3
	| expression3 t_leftshift expression4
	| expression3 t_rightshift expression4
	;

expression5:
	expression4
	| expression4 t_greater_than expression5
	| expression4 t_greater_than_or_equal_to expression5
	| expression4 t_less_than expression5
	| expression4 t_less_than_or_equal_to expression5
	;

expression6:
	expression5
	| expression5 t_equal_to expression6
	| expression5 t_not_equal_to expression6
	;

expression7: expression6 | expression6 t_bitwise_and expression7;
expression8: expression7 | expression7 t_bitwise_xor expression8;
expression9: expression8 | expression8 t_bitwise_or expression9;
expressionA: expression9 | expression9 t_logical_and expressionA;
expressionB: expressionA | expressionA t_logical_or expressionB;
expressionC: expressionB | expressionB t_question_mark expressionB t_colon expressionC;

expression: expressionC;

elseifs: %empty
	| t_elif expression statements elseifs {TODO;}
	| t_else t_newline statements {TODO;}
	;

if_statement:
	t_pound t_if expression t_newline statements elseifs t_endif {TODO;};

optional_expression: %empty | expression {TODO;};

define_statement: t_pound t_define t_identifier optional_expression t_newline {dpvs($3); TODO;};

macro_parameters: %empty | t_identifier t_comma macro_parameters {dpvs($1); TODO;};

macro_statement: t_pound t_define t_identifier t_open_paren macro_parameters
	t_close_paren optional_expression t_newline {dpvs($3); TODO;};

macro_invocation_parameters: %empty | expression t_comma macro_invocation_parameters {TODO;};

macro_invocation: t_identifier t_open_paren macro_invocation_parameters t_close_paren
	{
		if ((*error = new_macro_invocation_statement(&$$, $1)))
			YYABORT;
	};

include_statement: t_pound t_include expression t_newline {TODO;};

statement:
	t_integer_literal {TODO;}
	| t_character_literal {TODO;}
	| t_string_literal {TODO;}
	| t_identifier {dpvs($1); TODO;}
	| token_statement {$$ = (struct statement*) $1;}
	| if_statement {TODO;}
	| define_statement {TODO;}
	| macro_statement {TODO;}
	| macro_invocation {$$ = (struct statement*) $1;}
	| include_statement {TODO;}
	;

statements
	: %empty {$$ = NULL;}
	| statement statements { $1->next = $2, $$ = $1; }
	;

start: statements { *result = $1; };

%%






















