
struct string_array;

struct cmdln_flags
{
	const char* output_file;
	
	struct string_array* inputs;
	
	struct string_array* defines;
	struct string_array* include_paths;
};

