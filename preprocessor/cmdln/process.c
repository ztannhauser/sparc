
#include <getopt.h>

#include <error.h>
#include <debug.h>

#include <defines/argv0.h>

#include <memory/smalloc.h>

#include <string_array/struct.h>
#include <string_array/new.h>
#include <string_array/append.h>
#include <string_array/free.h>
#include <string_array/free_each.h>

#include "struct.h"
#include "process.h"

static struct option long_options[] = {
	{0,         0,                 0,  0}
};

int process_cmdln(struct cmdln_flags** out, int argc, char* const* argv)
{
	int error = 0;
	ENTER;
	
	const char* output_file = NULL;
	
	struct string_array* inputs;
	
	struct string_array* defines;
	struct string_array* include_paths;
	
	error = 0
		?: new_string_array(&inputs)
		?: new_string_array(&defines)
		?: new_string_array(&include_paths);
	
	int opt, option_index;
	while (!error && (opt = getopt_long(argc, argv, "o:D:I:", long_options, &option_index)) != -1)
	{
		switch (opt ?: option_index)
		{
			case 0:
				TODO;
				break;
			
			case 'o': output_file = optarg; break;
			
			case 'D':
				TODO; // append to preprocessor_args
				break;
			
			case 'I':
				TODO; // append to preprocessor_args
				break;
			
			default:
			{
				fprintf(stderr, "Usage: %s [args]\n", argv0);
				error = e_bad_command_line_args;
				break;
			}
		}
	}
	
	while (!error && optind < argc)
		error = string_array_append(inputs, argv[optind++]);
	
	struct cmdln_flags* flags = NULL;
	
	if (!error)
		error = smalloc((void**) &flags, sizeof(*flags));
	
	if (!error)
	{
		flags->output_file = output_file;
		
		flags->inputs = inputs, inputs = NULL;
		
		flags->defines = defines, defines = NULL;
		flags->include_paths = include_paths, include_paths = NULL;
		
		*out = flags;
	}
	
	free_string_array(inputs);
	free_string_array(defines);
	free_string_array(include_paths);
	
	EXIT;
	return error;
}




















