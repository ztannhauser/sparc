
#include <debug.h>

#include <string_array/free.h>

#include "struct.h"
#include "free.h"

void free_cmdln_flags(struct cmdln_flags* flags)
{
	ENTER;
	
	if (flags)
	{
		free_string_array(flags->inputs);
		
		free_string_array(flags->defines);
		free_string_array(flags->include_paths);
		
		free(flags);
	}
	
	EXIT;
}

