
#include <debug.h>
#include <error.h>

#include <defines/argv0.h>

#include "srealloc.h"

int srealloc(void** out, size_t size)
{
	int error = 0;
	ENTER;
	
	assert(out);
	
	void* oldptr = *out;
	void* newptr = realloc(oldptr, size);
	
	if (!size && !newptr)
	{
		fprintf(stderr, "%s: realloc(%lu): %m\n", argv0, size);
		error = e_out_of_memory;
	}
	else
		*out = newptr;
	
	EXIT;
	return error;
}

