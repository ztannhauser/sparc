
enum error
{
	e_success,
	e_syscall_failed,
	e_bad_command_line_args,
	e_out_of_memory,
	e_open_input_failed,
	e_open_output_failed,
	e_subcommand_failed,
	e_bison_error
};

